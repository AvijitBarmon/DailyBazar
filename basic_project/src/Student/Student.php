<?php
namespace App\Student;
if(!isset($_SESSION)){
    session_start();
}
use App\Connection;
use PDO;
use PDOException;



class Student extends Connection
{
    private $title;
    private $category;
    private $description;
    private $image;
    private $price;
    private $id;

    public function set(array $data)
    {

        if (array_key_exists('title', $data)) {


            $this->title = $data['title'];

        }
        if (array_key_exists('category', $data)) {
            $this->category = $data['category'];
        }
        if (array_key_exists('description', $data)) {
            $this->description = $data['description'];
        }


        if (array_key_exists('image', $data)) {
            $this->image = $data['image'];
        }

        if (array_key_exists('price', $data)) {
            $this->price = $data['price'];
        }


        if (array_key_exists('id', $data)) {
            $this->id = $data['id'];
        }


        return $this;
    }

    public function store()
    {
        try {
            $stmt = $this->con->prepare("INSERT INTO `bazar`(`title`,`category`,`description`,`image`,`price`,`unique_id`) 
                                                    VALUES(:n,:d,:a,:b,:m,:unique_id)");

            $result = $stmt->execute(array(
                ':n' => $this->title,
                ':d' => $this->category,
                ':a' => $this->description,
                ':b' => $this->image,
                ':m' => $this->price,
                ':unique_id' =>md5(time())
            ));
            if ($result) {
                $_SESSION['insert'] = 'Data successfully Inserted !!';
                header('location: index.php');
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
    }


    public function index(){
        try {

            $stm =  $this->con->prepare("SELECT * FROM `bazar` WHERE `deleted_at` = '0000-00-00 00:00:00'");

            $stm->execute();
            return $stm->fetchAll(PDO::FETCH_ASSOC);

        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
    }

    public function view($id)
    {
        try {
            $stmt = $this->con->prepare("SELECT * FROM `bazar`WHERE unique_id = :id");
            $stmt->bindValue(':id',$id, PDO::PARAM_INT);
            $stmt->execute();
            return $stmt->fetch(PDO::FETCH_ASSOC);

        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
    }


    public function delete($id){
        try {

            $stm =  $this->con->prepare("DELETE FROM `bazar` WHERE unique_id = :id");
            $stm->bindValue(':id', $id, PDO::PARAM_STR);
            $stm->execute();
            if($stm){
                $_SESSION['delete'] = 'Data successfully Deleted !!!';
                header('location:index.php');
            }

        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
    }




    public function update()
    {
        try {
            $stmt = $this->con->prepare("UPDATE `commerce`.`bazar` SET `title` = :title, `category` = :category, `description` = :description, `image` = :image, `price` = :price WHERE `bazar`.`unique_id` = :id;");
            $stmt->bindValue(':title', $this->title, PDO::PARAM_INT);
            $stmt->bindValue(':category', $this->category, PDO::PARAM_INT);
            $stmt->bindValue(':description', $this->description, PDO::PARAM_INT);
            $stmt->bindValue(':image', $this->image, PDO::PARAM_INT);
            $stmt->bindValue(':price', $this->price, PDO::PARAM_INT);
            $stmt->bindValue(':id', $this->id, PDO::PARAM_INT);
            $stmt->execute();
            if ($stmt) {
                $_SESSION['update'] = 'Data successfully Updated !!';
                header('location: index.php');
            }

        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
    }




    public function tmp_delete($id){
        try {

            $stm =  $this->con->prepare("UPDATE `bazar` SET `deleted_at` = NOW() WHERE unique_id = :id");
            $stm->bindValue(':id', $id, PDO::PARAM_STR);
            $stm->execute();
            if($stm){
                $_SESSION['delete'] = 'Data successfully Deleted !!!';
                header('location:index.php');
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
    }

    public function restore($id){
        try {

            $stm =  $this->con->prepare("UPDATE `bazar` SET `deleted_at` = '0000-00-00 00:00:00' WHERE unique_id = :id");
            $stm->bindValue(':id', $id, PDO::PARAM_STR);
            $stm->execute();
            if($stm){
                $_SESSION['delete'] = 'Data Restore successfully !!!';
                header('location:trash.php');
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
    }

    public function trash(){
        try {

            $stm =  $this->con->prepare("SELECT * FROM `bazar` WHERE `deleted_at` != '0000-00-00 00:00:00'");

            $stm->execute();
            return $stm->fetchAll(PDO::FETCH_ASSOC);

        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
    }




    }








