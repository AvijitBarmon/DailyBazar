<?php
include_once '../include/header.php';

include_once '../../vendor/autoload.php';

$student = new \App\Student\Student();
$student= $student->view($_GET['id']);


?>

    <div id="page-wrapper" style="min-height: 349px;">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header"></h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>



        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Product Details
                    </div>
                    <div class="panel-body">
                        <dl>
                            <dt>Product Title</dt>
                            <dd><?php echo $student['title']?></dd>
                            <dt>Category</dt>
                            <dd><?php echo $student['category']?></dd>
                            <dt>Description</dt>
                            <dd><?php echo $student['description']?></dd>
                            <dt>Image</dt>
                            <dd><img  class="img-responsive" width="100" src="view/uploads/<?php echo $student['image']?>"</dd>
                            <dt>Product_price</dt>
                            <dd><?php echo $student['price']?></dd>
                            <a href="view/student/pdf.php?id=<?php echo $student['unique_id']?>">Download</a>
                        </dl>
                    </div>
                </
                <!-- /.panel -->
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
    </div>




        <div style="position: fixed; top: 200px; right: 50px; z-index: 11111">
            <?php
            if(isset($_SESSION['msg'])){
                echo "<div class='alert alert-success'>".$_SESSION['msg']."</div>";
                session_unset();
            }
            ?>
        </div>





<?php
include_once '../include/footer.php';
?>