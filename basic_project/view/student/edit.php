<?php
include_once '../include/header.php';

include_once '../../vendor/autoload.php';
$student = new \App\Student\Student();

$student = $student->set($_GET)->view();

?>

    <div id="page-wrapper" style="min-height: 349px;">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Update</h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Product Details
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-lg-8 col-md-offset-2">
                                <form role="form" action="view/student/update.php" method="POST" enctype="multipart/form-data">
                                    <div class="form-group">
                                        <label>Product Title</label>
                                        <input value="<?php echo $student['title']?>" name="title" class="form-control">
                                        <input type="hidden" value="<?php echo $student['unique_id']?>" name="id"
                                               class="form-control">

                                        <input type="hidden" value="<?php echo $student['image']?>" name="image"
                                               class="form-control">
                                    </div>
                                    <div class="form-group">
                                        <label>Category</label>
                                        <select name="category" id="" class="form-control">
                                            <option>Select One</option>
                                            <option <?php echo ($student['category']=='Male')?'selected':'' ?> value="Male">Male</option>
                                            <option <?php echo ($student['category']=='Female')?'selected':'' ?> value="Female">Female</option>
                                            <option <?php echo ($student['category']=='Baby')?'selected':'' ?> value="Baby">Baby</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>Description</label>
                                        <textarea name="description"   class="form-control" rows="3"><?php echo $student['description']?></textarea>
                                    </div>


                                    <div class="form-group">
                                        <label for="">Upload Image</label>
                                        <input type="file" name="image">
                                    </div>
                                    <div style="padding-bottom: 25px">
                                        <img width="100" src="view/uploads/<?php echo $student['image']?>" alt="">
                                    </div>



                                        <div class="form-group">
                                        <label>Product Price</label>
                                        <input value="<?php echo $student['price']?>" name="price" class="form-control">



                                    <button type="reset" class="btn btn-default">Reset Button</button>
                                    <button type="submit" class="btn btn-info">Update</button>
                                </form>
                            </div>
                        </div>
                        <!-- /.row (nested) -->
                    </div>
                    <!-- /.panel-body -->
                </div>
                <!-- /.panel -->
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
    </div>

<?php
include_once '../include/footer.php';
?>